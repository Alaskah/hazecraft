# HazeCraft
## Versão Atual: BETA 6.3.3.1

[HazeForum](https://hazeforum.com "Clique e acesse agora!")  
<suporte@hazeforum.com>


## ChangeLog

## APARTIR DAQUI : UPDATES EM TEMPO REAL  
 [NEW].[HAZE] - Initialized HZPanel  
 [VERSION].[PAYMENT SYSTEM] - Old Version: *Beta v1.5* **,** new Version *Official v1.0*  
 [ADD/NEW] - Modern Cart v1.5 by: HazeCraft v6.3.2.1  
 [DEL] - OneCart v1.0 by: HazeCraft v5.0  
 [ADD/NEW] - License Required - Get one at the Haze Store [FINISH]  
 [ADD] - Login, results: 10/10 [FINISH]  
 [ADD/NEW] - Initialized **function** : *Logar().usuario.class*, results: 10/10 [FINISH]  
 [ADD/NEW] - Initialized **function** : *SenhaCrypt().usuario.class*, results: 10/10 [FINISH]  
 [ADD/NEW] - Initialized **function** : *Logado().usuario.class*, results: 10/10 [FINISH]  
 [ADD/NEW] - Initialized **class** : *usuario.class*, results: 10/10 [FINISH]  
 [ADD] - Articles, results: 10/10 [FINISH]  
 [DTB] - Added colums in **produtos** : *stock* and *avaliable*, results: 10/10 [FINISH]  
 [ADD/NEW] - Initialized **class** : *produtos.class*, results: 10/10 [FINISH]  
 [DEL] - Deleted from **hazecraft.class** : *promotions_header* and *promotions_side*, results: 10/10 [FINISH]  
 [ADD] - Created conditions from **view().produtos.class** for *stock* and *avaliable*, results: 10/10 [FINISH]  
 
## UPDATES ANTIGOS

 [NEW] - Toda a Base do Sistema foi e esta sendo reestruturada.  
 [NEW] - Pastas Agora estão bem mais organizadas  
 [NEW] - Funções e Classes Mais Produtivas  
 [NEW] - Alguns Sistemas passaram a ser funções e Algumas Classes passaram a ser Funções  
 [NEW] - Classes e Funções desnesessárias Removidas  
 [NEW] - Funções Aprimoradas  
 [NEW] - Bulma Removido, Está sendo reefeito tudo com Bootsrap, Informações no final do Tópico.  
 [NEW] - Css Mais leve, separado e organizado.  
 [NEW] - Muitos Diretorios Inuteis Foram Removidos.  
 [NEW] - Prints( Já que a demo está off )  
 

# Versões BETA descontinuadas e finalizadas:

## Apartir Daqui os logs estão desorganizados!

## Beta 5.0

 [NEW] - Tema Para Servidores Descontinuado;
 [NEW] - Agora o tema da Loja será: Vendas de contas minecraft e etc;
 [ADD] - Widget Slider Agora em PDO;
 [ADD] - Promoções;
 [NEW] - Melhoria nos sistemas principais (POO);
 [NEW] - Redução no número de arquivos;
 [NEW] - SEO Em Andamento;
 [ADD] - Novo Header Elegante com DESTAQUE ( Anuncio de produto logo no header, confira no print 1)

## Betas 4.0 e 4.1

 [ADD] - Painel Admin ( Inicializado );
 [ADD] - Noticias ( Finalizado );
 [ADD] - Widget Slider (Finalizado); // Não é possivel remove-lo.
 [ADD] - Produtos (Index);
 [DEL] - Log ( Removido );
 [NEW] - Atualizações ( Finalizado );
 [ADD] - Login/ Register no usuário ( Base do Sistema Em Desenvolvimento );
 [NEW] - Arquivos Organizados por pastas;
 [ADD] - Configuração total do site ( Nome, link ... ) no arquivo de configurações;
 [DEL] - Mega Footer Removido, como na versão anterior.

## Beta 3.0

 [ADD] - NavBar com background mais bonito que a versão anterior
 [ADD] - Borda da NavBar Espinhada(Triangulos)
 [ADD] - Box - Copie nosso ip
 [ADD] - Produtos - Visual Moderno
 [ADD] - Footer Simples porém estiloso

